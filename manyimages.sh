#!/bin/bash
trap out SIGINT
out() {
   echo
   echo Exiting...
   exit 0
}

if ! [ -f "functions.h" ]; then
  echo "didn't find functions.h file. `make` or run ./mkfunctions.py
  before running this script"
  exit 1
fi

num=$(cat functions.h | grep '#define FUNCN' | cut -d' ' -f 3)

mkdir -p data
mkdir -p img
mkdir -p info
mkdir -p time

size="--width 9000 --height 9000"
for (( i=0; i < $num; i++ )); do
  echo -ne "\nGenerating $i.bin... "
  /usr/bin/time -o time/$i.time ./newton -o data/$i.bin $size -f f$i > info/$i.txt
  echo -ne "Done\n  Time: "
  cat time/$i.time | head -1
  echo -ne "Generating $i.png... "
  /usr/bin/time -o time/$i-img.time ./mkimage.py data/$i.bin img/$i.png
  echo -ne "Done\n  Time: "
  cat time/$i-img.time | head -1
done
