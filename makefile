CFLAGS=-Wall -Wpedantic -O3
LDLIBS=-lm
CC=gcc

.PHONY: all
all: ponto-fixo newton

image.png: newton mkimage.py functions.c functions.h
	./newton --output output.bin
	./mkimage.py output.bin image.png

ponto-fixo: ponto-fixo.h
newton: newton.h functions.o

newton: functions.o

functions.o: functions.c functions.h

functions.c functions.h: functions.c.jinja2 functions.h.jinja2
	./mkfunctions.py

relatorio.pdf: relatorio.tex
	pdflatex relatorio.tex

.PHONY: clean
clean:
	rm -f *.o ponto-fixo *.aux *.log relatorio.pdf output.bin image.png
