#include "ponto-fixo.h"

/* Calculating exp(x) - 2x² with fixed point iteration. We know the
 * 3 roots are in [-1, 3].
 *
 * g_4: [, 0]
 * g_3: [0, 0.5]
 * g_2: [0.5, 2]
 * g_1: [2, ]
 */

int main(int argc, char **argv) {
  double x0, tol;
  double (*g)(double);
  int (*criteria)(double, double, double);

  tol = 1e-15;
  criteria = abtol;

  if (argc <= 4) {
    if (argc > 1)
      x0 = atof(argv[1]);
    if (argc > 2) {
      if (!strcmp(argv[2], "atol"))
        criteria = abtol;
      else if (!strcmp(argv[2], "rtol"))
        criteria = rtol;
      else if (!strcmp(argv[2], "ftol"))
        criteria = ftol;
      else if (!strcmp(argv[2], "xtol"))
        criteria = xtol;
      else
        goto printhelp;
    }
    if (argc > 3) {
      tol = atof(argv[3]);
      if (tol <= 0)
        goto printhelp;
    }
    else
      goto printhelp;
  }
  else
    goto printhelp;

  if (x0 <= 0) {
    g = g_4;
  }
  else if (x0 > 0 && x0 < 0.5) {
    g = g_3;
  }
  else if (x0 > 0.5 && x0 <= 2) {
    g = g_2;
  }
  else {
    g = g_1;
  }

  printf("%f is a root of f\n", ponto_fixo(x0, g, criteria, tol));
  return 0;

  printhelp:
  printf("This program finds one of the roots of the function f(x) = "
         "exp(x) - 2x^2\n\n"
         "Usage: ./ponto-fixo x0 [criterion] [tol]\n"
         "\n"
         "* x0 is the initial value for the iteration.\n"
         "* criterion is the criterion method to use. "
         "The options are as follows:\n"
         "  - atol (absolute tolerance)\n"
         "  - rtol (relative tolerance)\n"
         "  - ftol (\"function\" tolerance, |f(x_k) - f(x_k-1)| < tol)\n"
         "  - xtol (|x_k - x_k-1| < tol(1 + |kx|))\n"
         "\n"
         "  and defaults to rtol.\n"
         "\n"
         "* tol is the positive value of the tolerance and defaults "
         "to 1e-15\n");
  return 1;
}



double ponto_fixo(double x0,
                  double (*g)(double),
                  int(*criteria)(double, double, double),
                  double tol)
{
  double x_k = g(x0), x_j = x0;

  while (!criteria(x_j, x_k, tol)) {
    x_j = x_k;
    x_k = g(x_k);
  }

  return(x_k);
}




double f(double x) {
  return (exp(x) - 2*pow(x, 2));
}



int abtol(double x_j, double x_k, double tol) {
  return(fabs(x_j - x_k) < tol);
}

int rtol(double x_j, double x_k, double tol) {
  return(fabs((x_j - x_k)/x_k) < tol);
}

int ftol(double x_j, double x_k, double tol) {
  return(fabs(f(x_j) - f(x_k)) < tol);
}

int xtol(double x_j, double x_k, double tol) {
  return(fabs(x_j - x_k)/(1 + fabs(x_k)) < tol);
}




double g_1(double x) {
  return(2 * log(x*sqrt(2)));
}

double g_2(double x) {
  return(x + exp(x) - 2*pow(x, 2));
}

double g_3(double x) {
  return(sqrt(exp(x)/2));
}

double g_4(double x) {
  return(-sqrt(exp(x)/2));
}
