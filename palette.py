#!/usr/bin/env python3

# This script produces a pallete. If used as a library, it should serve
# the function gen_palette, which returns a procedurally generated
# palette (list of rgb tuples). If called as a script, the usage is
#
# $ ./palette.py [outfile colnumber]
#
# where outfile is the name of the png file to be created, with which
# the palette can be visualized (default image.png), and colnumber is
# the number of color to use (default 20)

# gen_palette
#
# colnum:

import png
import colorsys
import random as r
import sys


def gen_palette(colnum = 20, shuffle = True,
                lumstart = 0.3, lumend = 1, sat = 0.7):

    if type(colnum) == list:
        most_used = colnum
        colnum = len(colnum)
    elif type(colnum) == int:
        most_used = [i for i in range(colnum)]
    else:
        raise TypeError("argument colnum must be int or list")


    # Define the number of hues depending on the number of colors
    s = 0
    s = 0
    huesn = 1
    for i in range(colnum):
        s += i * 2
        if colnum <= s:
            huesn = i
            break
    # huesn = 1 if colnum == 1 else 2


    hues = [(r.uniform(0, 0.54) - 0.04) % 1] # [0, .50] U [.96, 1]
    # if huesn > 1:
    #     hues.append(hues[0] + 0.2)

    # Define the hues. Mix complementary (+ 0.5) and adjacent (+ 0.1)
    for i in range(1, huesn):
        if i % 2 == 1:
            hues.append((hues[i - 1] + 0.5) % 1)
        else:
            hues.append((hues[i - 2] + 0.1) % 1)


    # how many shades variations each hue shall get, such that
    # sum(huesizes) == colnum
    huesizes = [colnum // huesn] * huesn
    for i in range(colnum % huesn):
        huesizes[i] += 1

    most_used.reverse()
    hls = [None] * colnum
    for h in range(huesn):
        for i in range(huesizes[h]):
            lum = ((lumend - lumstart)/colnum * i) + lumstart
            # |(n - 0.28) % 1 - 0.5 |
            # the farther from 0.78 (an ugly purple), bigger the saturation
            s = abs(((hues[h] - 0.28) % 1) - 0.5) * 1.6 + 0.2
            hls[most_used[h + i * huesn] - 1] = (hues[h], lum , s * sat)


    palette = [(0, 0, 0)]
    palette += [tuple([int(255 * c) for c in colorsys.hls_to_rgb(*h)]) for h in hls]

    # if shuffle:
        # r.shuffle(palette)
    return palette


if __name__ == "__main__":

    outfile = "image.png"
    colnum = 20
    if len(sys.argv) > 1:
        outfile = sys.argv[1]
    if len(sys.argv) > 2:
        colnum = int(sys.argv[2])

    width = colnum *15
    height = width
    data = [[i//(width//colnum)  for i in range(width) ] for j in range(height)]

    palette = gen_palette(colnum, False)

    w = png.Writer(width=width, height=height, palette=palette)
    with open(outfile, 'wb') as f:
        w.write(f, data)
