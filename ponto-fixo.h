#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/* ponto-fixo x0 [criterion] [tol] */

/* x0 is the initial value for the iteration
 * criterion is the criterion method to use. The options are as follows:
   - atol (absolute tolerance)
   - rtol (relative tolerance)
   - ftol ("function" tolerance, |f(x_k) - f(x_k-1)| < tol)
   - xtol (|x_k - x_k-1| < tol(1 + |kx|))

   and defaults to rtol.

 * tol is the value of the tolerance and defaults to 1e-15
 */

/* The fixed point iteration. */
double ponto_fixo(double x0,
                  double (*g)(double),
                  int(*criteria)(double, double, double),
                  double tol);

/* The function to be evaluated */
double f(double x);

/* Termination criteria */
/* Return true if criterion is met */
int abtol(double x_j, double x_k, double tol);
int rtol(double x_j, double x_k, double tol);
int ftol(double x_j, double x_k, double tol);
int xtol(double x_j, double x_k, double tol);

/* "g" functions */
double g_1(double x);
double g_2(double x);
double g_3(double x);
double g_4(double x);

