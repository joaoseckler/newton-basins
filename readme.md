# Newton Basins

This set of scripts is intended for procedurally generating newton
basins[1]. The script `mkfunctions.py` generates a library of
procedurally generated differentiable functions and their derivatives.
`./newton` produces a data file with information for generating an
image, which is to be read by the script `mkimage.py`, which chooses a
random color for each basin and outputs a `png` image.

To produce one image, try, after `make`ing:

```
./mkfunctions 1
./newton -o output.bin
./mkimage output.bin image.png
```

which is exactly what `make image.png` does.

To produce a hundred images, you can use the script

`./manyimages.sh`

which may take several hours to complete.

See also `./newton --help` for more options on generating the images.


![0](relatorio.d/0.png)
![4](relatorio.d/4.png)
![15](relatorio.d/15.png)
![16](relatorio.d/16.png)
![35](relatorio.d/35.png)
![39](relatorio.d/39.png)
![42](relatorio.d/42.png)
![43](relatorio.d/43.png)
![47](relatorio.d/47.png)
![55](relatorio.d/55.png)
![68](relatorio.d/68.png)
![74](relatorio.d/74.png)
![90](relatorio.d/90.png)
![99](relatorio.d/99.png)


## MAC210

This work is an assigment of class MAC0210 (Numerical Methods) at
IME-USP. The `ponto-fixo` is an experiment with fixed point iteration
with the function exp(x) - 2x². It is not connected to the newton
basins.


## Dependencies
* Make
* gcc
* python3

*C libraries*
* math.h
* complex.h
* getopt.h
* string.h

*Python3 libraries*
* sympy
* jijna2
* pypng
* random
* re

## Todo

* Function generation is poor because it can only adds terms. You could
  never get sin(x)x^2, for example, only sin(x) + x^2. Implement
  division and multiplication of terms by auxiliary functions.

* Newton.c: function abstraction makes no sense. `parse_args` should be in
  `main`, file should be created and destroy in main, etc


[1] https://mathcs.clarku.edu/~djoyce/newton/newton.html
