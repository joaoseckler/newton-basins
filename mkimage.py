#!/usr/bin/env python3

# This script works as a complement to ./newton. It takes an file name
# as it's firt argument (default output.bin). This file should be a
# binary file with rows of positive integers representing lines of an
# image. This script interprets each of these integers as a random color
# and produces a png image as output. The name of the output is the
# second argument to the script, which defaults to image.png

import png
from random import randint
import sys
from itertools import chain
from collections import Counter
import palette as p

infile = "output.bin"
outfile = "image.png"
if len(sys.argv) > 1:
    infile = sys.argv[1]
if len(sys.argv) > 2:
    outfile = sys.argv[2]



with open(infile, 'rb') as f:
    data = f.read()


data = [[int(i) for i in d] for d in data.split(b'\xff')]
data.pop()

# List of most used ints
c = [k for k, v in Counter(chain.from_iterable(data)).most_common()]
if 0 in c:
    c.remove(0)

palette = p.gen_palette(c)

w = png.Writer(width=len(data[0]), height=len(data), palette=palette)

with open(outfile, 'wb') as f:
    w.write(f, data)


