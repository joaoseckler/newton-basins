#include "newton.h"

int main(int argc, char **argv) {
  double tol;
  int (*criterion)(double complex, double complex, double,
                    double complex(*f)(double complex));
  int p[2], maxit;
  complex double u, l;
  char * outfile, * def = "output.bin";
  outfile = def;
  double complex(*f)(double complex);
  double complex(*df)(double complex);
  char *fstring, *dfstring;

  parse_args(argc, argv, &f, &df, &tol, &criterion, p, &maxit, &u, &l,
             &outfile, &fstring, &dfstring);

  printf("** %s - information **\n\n", outfile);
  printf("Image size: %d x %d\n", p[0], p[1]);
  printf("Ranging from %f + %fi to %f + %fi\n", creal(u), cimag(u),
                                                creal(l), cimag(l));
  printf("\nUsed function:\n%s\n\n", fstring);
  printf("Derivative:\n%s\n\n", dfstring);
#if PRINT_ITERATIONS == 1
  printf("iterations:\n");
#endif

  newton_basins(u, l, p, maxit, f, df, criterion, tol, &outfile);


  if (outfile != def)
    free(outfile);
  return 0;
}


void newton_basins(double complex u,
                   double complex l,
                   int *p, int maxit,
                   double complex(*f)(double complex),
                   double complex(*df)(double complex),
                   int (*criterion)(double complex, double complex, double,
                                    double complex(*f)(double complex)),
                   double complex tol,
                   char ** outfile)
{
  double complex roots[MAX_ROOTS];
  for (int i = 0; i < MAX_ROOTS; i++)
    roots[i] = NAN;

  double rstep = (creal(l) - creal(u))/p[1];
  double istep = (cimag(l) - cimag(u))/p[0];
  double complex x = u, r;
  int k, iterations = 0, n = 0;
  double itavg = 0;

  FILE *fp = fopen(*outfile, "wb");
  if (!fp) {
    fprintf(stderr, "Unable to open specified output file. Exiting...\n");
    fclose(fp);
    exit(1);
  }

  for (int i = 0; i < p[1]; i++) {
    for (int j = 0; j < p[0]; j++) {
      r = newton(x, f, df, criterion, tol, maxit, &iterations);

#if PRINT_ITERATIONS == 1
      printf("%d\n", iterations);
#endif
      itavg = (itavg * (n) + iterations) / (n + 1);
      n++;

      if (isnan(creal(r)))
        fputc(0, fp);

      else {
        for(k = 0; !isnan(creal(roots[k])) && k < MAX_ROOTS; k++) {
          if (criterion(roots[k], r, tol, f)) {
              fputc(k + 1, fp); /* 0 means divergent, thus k + 1 */
            r = NAN;
            break;
          }
        }

        if (!isnan(creal(r))) {
          roots[k] = r;
          fputc(k + 1, fp);
        }
      }

      x += istep * I;
    }
    x = creal(x) + rstep + cimag(u) * I;
    fputc(-1, fp);
  }

  fclose(fp);
  for(k = 0; !isnan(creal(roots[k])) && k < MAX_ROOTS; k++);
  printf("Average number of iterations: %f\n", itavg);
  printf("Number of roots found: %d\n", k);
}


double complex newton(double complex x0,
               double complex(*f)(double complex),
               double complex(*df)(double complex),
               int (*criterion)(double complex, double complex, double,
                                double complex(*f)(double complex)),
               double complex tol,
               int maxit,
               int *iterations)
{
  double complex x_k = (x0 - (*f)(x0)/(*df)(x0)), x_j = x0;
  int c = 0;

  while (!criterion(x_j, x_k, tol, f) && c < maxit) {
    x_j = x_k;
    x_k = x_k - (*f)(x_k)/(*df)(x_k);
    c++;
  }

  *iterations = c;
  if (c >= maxit)
    return NAN;
  return(x_k);
}

void parse_args(int argc, char ** argv,
                double complex(**f)(double complex),
                double complex(**df)(double complex),
                double *tol,
                int (**criterion)(double complex, double complex, double,
                                  double complex(*f)(double complex)),
                int *p, int *maxit,
                double complex *u, double complex *l,
                char ** outfile,
                char ** fstring, char ** dfstring) {
  int c;
  static struct option long_options[] = {
    {"tol",       required_argument, 0, 'a'},
    {"criterion", required_argument, 0, 'b'},
    {"width",     required_argument, 0, 'c'},
    {"height",    required_argument, 0, 'd'},
    {"startr",    required_argument, 0, 'e'},
    {"starti",    required_argument, 0, 's'},
    {"endr",      required_argument, 0, 'g'},
    {"endi",      required_argument, 0, 'i'},
    {"maxit",     required_argument, 0, 'm'},
    {"output",    required_argument, 0, 'o'},
    {"function",  required_argument, 0, 'f'},
    {"help",      no_argument,       0, 'h'},
    {0, 0, 0, 0}};

  /* getopt_long stores the option index here. */
  int index = 0;
  int i;

  *tol = 1e-15;
  *criterion = xtol;
  p[1] = p[0] = 0;
  *u = -1 - I * 1;
  *l = 1 + I * 1;
  *maxit = 100;

  double complex (*flist[FUNCN])(double complex) = F_LIST;
  double complex (*dflist[FUNCN])(double complex) = DF_LIST;
  char * flist_string[FUNCN] = F_STRING_LIST;
  char * dflist_string[FUNCN] = DF_STRING_LIST;
  char * fnames[FUNCN] = FNAMES;
  *f = flist[0];
  *df = dflist[0];
  *fstring = flist_string[0];
  *dfstring = dflist_string[0];

  while ((c = getopt_long(argc, argv, "ho:f:", long_options, &index)) != -1) {
    switch (c) {
      case 'a':
        *tol = atof(optarg);
        break;

      case 'b':
        if (!strcmp(optarg, "atol"))
          *criterion = abtol;
        else if (!strcmp(optarg, "rtol"))
          *criterion = rtol;
        else if (!strcmp(optarg, "ftol"))
          *criterion = ftol;
        else if (!strcmp(optarg, "xtol"))
          *criterion = xtol;
        else
          print_help();
        break;

      case 'c':
        p[0] = atoi(optarg);
        break;

      case 'd':
        p[1] = atoi(optarg);
        break;

      case 'e':
        *u = *u + atof(optarg);
        break;

      case 's':
        *u = *u + atof(optarg) * I;
        break;

      case 'g':
        *l += atof(optarg);
        break;

      case 'i':
        *l += atof(optarg) * I;
        break;

      case 'h':
        print_help();
        break;

      case 'o':
        *outfile = malloc(sizeof(char) * (strlen(optarg) + 1));
        strcpy(*outfile, optarg);
        break;

      case 'm':
        *maxit = atoi(optarg);
        break;

      case 'f':
        for (i = 0; i < FUNCN; i++) {
          if (!strcmp(fnames[i], optarg)) {
            *f = flist[i];
            *df = dflist[i];
            *fstring = flist_string[i];
            *dfstring = dflist_string[i];
            break;
          }
        }
        if (i == FUNCN) {
          fprintf(stderr, "Unvalid function name.\n");
          exit(1);
        }
        break;


      case '?':
        /* getopt_long already printed an error message. */
        abort();
        break;

      default:
        break;
      }
    }


  if (*tol <= 0) {
    fprintf(stderr, "Tol value given is nonpositive.\n");
    exit(1);
  }

  if (creal(*l) == creal(*u)) {
    fprintf(stderr, "startr and endr coincide.\n");
    exit(1);
  }

  if (cimag(*l) == creal(*u)) {
    fprintf(stderr, "starti and endi coincide.\n");
    exit(1);
  }

  if (p[0] <= 0) {
    if (p[1] > 0)
      p[0] = p[1];
    else
      p[0] = p[1] = 600;
  }
  else if (p[1] <= 0)
    p[1] = p[0];

  if (*maxit <= 0) {
    fprintf(stderr, "maxit value given is nonpositive.\n");
    exit(1);
  }
}

void print_help()
{
  printf(
"newton - output newton basins data for image generation\n\
\n\
./newton [OPTION]\n\
\n\
newton produces a file output.bin with data to generate an image. This\n\
image maps complex numbers to different colors.  Each color is\n\
associated to the set of complex numbers C for which the same root of\n\
the function f(x) = x^5 - x^3 is found when calculated by newton's\n\
method with c in C as input. The output file consists of series of bytes\n\
with values in {1, 2, 3...} separated by bytes with value -1. Each\n\
series represents one line of pixels.\n\
\n\
\n\
  --criterion\n\
    criterion method to use in newton's method. Options are as follows:\n\
     - atol (absolute tolerance)\n\
     - rtol (relative tolerance)\n\
     - ftol (\"function\" tolerance, |f(x_k) - f(x_k-1)| < tol)\n\
     - xtol (|x_k - x_k-1| < tol(1 + |kx|))\n\
    default is xtol.\n\
\n\
  --tol\n\
    value of tolerance, used by the chosen criterion. Defaults to 1e-15.\n\
    Must be positive.\n\
\n\
  --maxit\n\
    maximum number of iterations before newton's method assumes it is\n\
    diverging\n\
\n\
  --width\n\
  --height\n\
    specify the size of the image\n\
\n\
  --startr, --starti, --endr, --endi\n\
    specify the set C such that C = { c : startr < real(c) < endr,\n\
                                          starti < im(c) < endi }\n\
\n\
  -h, --help\n\
  -o, --output\n\
    output file. Default is output.bin\n\
");
  exit(1);
}

/* Criteria */
int abtol(double complex x_j,
         double complex x_k,
         double tol,
         double complex(*f)(double complex))
{
  return(cabs(x_j - x_k) < tol);
}
int rtol(double complex x_j,
         double complex x_k,
         double tol,
         double complex(*f)(double complex))
{
  return(cabs((x_j - x_k)/x_k) < tol);
}
int ftol(double complex x_j,
         double complex x_k,
         double tol,
         double complex(*f)(double complex))
{
  return(cabs((*f)(x_j) - (*f)(x_k)) < tol);
}

int xtol(double complex x_j,
         double complex x_k,
         double tol,
         double complex(*f)(double complex))
{
  return(cabs(x_j - x_k)/(1 + cabs(x_k)) < tol);
}

