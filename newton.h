#include <math.h>
#include <complex.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include "functions.h"

#define PRINT_ITERATIONS 0

/*

"newton - output newton basins data for image generation

./newton [OPTION]

newton produces a file output.bin with data to generate an image. This
image maps complex numbers to different colors.  Each color is
associated to the set of complex numbers C for which the same root of
the function f(x) = x^5 - x^3 is found when calculated by newton's
method with c in C as input. The output file consists of series of bytes
with values in {1, 2, 3...} separated by bytes with value -1. Each
series represents one line of pixels.


  --criterion
    criterion method to use in newton's method. Options are as follows:
     - atol (absolute tolerance)
     - rtol (relative tolerance)
     - ftol ("function" tolerance, |f(x_k) - f(x_k-1)| < tol)
     - xtol (|x_k - x_k-1| < tol(1 + |kx|))
    default is xtol.

  --tol
    value of tolerance, used by the chosen criterion. Defaults to 1e-15.
    Must be positive.

  --maxit
    maximum number of iterations before newton's method assumes it is\n\
    diverging

  --width
  --height
    specify the size of the image

  --startr, --starti, --endr, --endi
    specify the set C such that C = { c : startr < real(c) < endr,
                                          starti < im(c) < endi }

  -h, --help
  -o, --output
    output file. Default is output.bin

*/

/* The maximum number of roots the function newton_basins is expected to
 * find (i.e. how many color at most the image will have, besides areas
 * that diverge) */
#define MAX_ROOTS 100

/* generate output data */
void newton_basins(double complex u,
                   double complex l,
                   int *p, int maxit,
                   double complex(*f)(double complex),
                   double complex(*df)(double complex),
                   int (*)(double complex, double complex, double,
                           double complex(*f)(double complex)),
                   double complex tol,
                   char ** outfile);

/* The newton method iteration. */
double complex newton(double complex x0,
              double complex(*f)(double complex),
              double complex(*df)(double complex),
              int (*)(double complex, double complex, double,
                      double complex(*f)(double complex)),
              double complex tol,
              int maxit,
              int *iterations);

/* /1* The function to be evaluated: x^5 - x^3 *1/ */
/* double complex f(double complex x); */

/* /1* The derivative of the function to be evaluated: 5x^4 - 3x^2 *1/ */
/* double complex df(double complex x); */

/* Termination criteria */
/* Return true if criterion is met */
int abtol(double complex x_j,
          double complex x_k,
          double tol,
          double complex(*f)(double complex));
int rtol(double complex x_j,
         double complex x_k,
         double tol,
         double complex(*f)(double complex));
int ftol(double complex x_j,
         double complex x_k,
         double tol,
         double complex(*f)(double complex));
int xtol(double complex x_j,
         double complex x_k,
         double tol,
         double complex(*f)(double complex));

/* Parse arguments with getopt */
void parse_args(int argc, char ** argv,
                double complex(**f)(double complex),
                double complex(**df)(double complex),
                double *tol,
                int (**criterion)(double complex, double complex, double,
                                  double complex(*f)(double complex)),
                int *p, int *maxit,
                double complex *u, double complex *l,
                char ** outfile,
                char ** fstring, char ** dfstring);

/* Helper function */
void print_help();
