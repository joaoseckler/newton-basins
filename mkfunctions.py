#!/usr/bin/env python3

# This script procedurally generates a library of functions and their
# derivatives in C. Routine gen_function will produce a differentiable
# sympy function. Then sympy.ccode does most of the job for us, except
# that it doesn't know the complex versions of the functions (ex: ctan
# instead of tan). The function "translate" corrects this and outputs
# the expression of a valid function in C. Then the templating engine
# jijna2 is used to produce the functions.c and functions.h files.

# Generating the function

# The following steps are taken to generate a function
# - The degree of the function is drawn
# - For each i ∈ { z ∈ ℤ : z < LIMIT }, a coefficient is drawn to x^i
# - A number of auxiliary functions is drawn. Auxiliary functions are
#   listed under "laux".
# - For each auxiliary function, a coefficient is drawn and, if
#   required, the other argument to that function. This function is
#   added to the main expression

from sympy import *
from random import randint, uniform, choice
import re
import jinja2
import sys

LIMIT = 10 # randomly generated numbers r will be |r| <= 10
FUNCN = 100 # Number of functions to randomly generate

if len(sys.argv) > 1:
    try:
        FUNCN = int(sys.argv[1])
    except ValueError:
        print("Please provide the number of functions to be generated. Default is 100. Exiting...")
        exit(1)

# List of available auxiliary functions. "pow" two times because we want
# the same chance of having x^c as c^x as any other.
laux = [ acos, acosh, asin, asinh, atan, atanh, cos, cosh, exp, log,
    sin, sinh, sqrt, tan, tanh, pow, pow, ]

# List of translations of auxiliary functions to complex versions in C
pyaux = ['acos', 'acosh', 'asin', 'asinh', 'atan', 'atanh', 'cos',
        'cosh', 'exp', 'log', 'sin', 'sinh', 'sqrt', 'tan', 'tanh',
        'pow', ]
caux = ['cacos', 'cacosh', 'casin', 'casinh', 'catan', 'catanh', 'ccos',
        'ccosh', 'cexp', 'clog', 'csin', 'csinh', 'csqrt', 'ctan',
        'ctanh', 'cpow', ]

x = symbols('x', complex=True)


def gen_function():
    ans = 0
    degree = randint(1, LIMIT)
    ans += sum([(x**i) * uniform(-LIMIT, LIMIT) for i in range(degree)])

    naux = randint(0, LIMIT)
    for i in range(naux):
        g = choice(laux)
        coef = uniform(-LIMIT, LIMIT)*x**i
        if g == pow:
            if randint(0, 1):
                ans += g(x, uniform(-LIMIT, LIMIT)) * coef
            else:
                ans += g(uniform(-LIMIT, LIMIT), x) * coef
        else:
            ans += g(x) * coef

    return ans

def translate(f):
    ''' Translate from sympy to C code'''
    a = str(ccode(f))
    for i in range(len(caux)):
        a,b = re.subn(r'(\W)' + pyaux[i] + r'(\W)',
                      r'\1' + caux[i] + r'\2',
                      a)
    return a

env = {'funcn': FUNCN}
env['functions'] = []
for i in range(FUNCN):
    f = gen_function()
    df = diff(f)
    b = '\\'
    env['functions'].append({
        'fname': 'f' + str(i),
        'fbody': translate(f),
        'dname': 'df' + str(i),
        'dbody': translate(df),
        'fstring': latex(f, mode='equation', itex=True).replace(b, b + b),
        'dfstring': latex(df, mode='equation', itex=True).replace(b, b + b),
        })


with open ('functions.c.jinja2', 'r') as f:
    templatec = jinja2.Template(f.read())
with open ('functions.h.jinja2', 'r') as f:
    templateh = jinja2.Template(f.read())

with open ('functions.c', 'w') as f:
    f.write(templatec.render(env))
with open ('functions.h', 'w') as f:
    f.write(templateh.render(env))


# C functions for complex numbers (from complex.h)
# cacos(double complex);
# cacosh(double complex);
# casin(double complex);
# casinh(double complex);
# catan(double complex);
# catanh(double complex);
# ccos(double complex);
# ccosh(double complex);
# cexp(double complex);
# clog(double complex);
# cpow(double complex, double complex);
# csin(double complex);
# csinh(double complex);
# csqrt(double complex);
# ctan(double complex);
# ctanh(double complex);
